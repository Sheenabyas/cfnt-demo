const { src, dest, watch, series, parallel } = require('gulp');
const sass = require('gulp-sass'), 
 concat = require('gulp-concat'), 
 uglify = require('gulp-uglify'), 
 postcss = require('gulp-postcss'), 
 autoprefixer = require('autoprefixer'), 
 cssnano = require('cssnano'),
replace = require('gulp-replace');


const files = { 
    scssPath: 'css/scss/soul.scss',
    jsPath: 'js/source/*.js'
}

function scssTask(){    
    return src(files.scssPath)
        .pipe(sass({
      includePaths: './css/scss/import/'
    })) 
        .pipe(postcss([ autoprefixer(), cssnano() ]))
        .pipe(dest('css')
    ); 
}

function jsTask(){
    return src([
        files.jsPath
        ])
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(dest('js')
    );
}


function watchTask(){
    watch([files.scssPath, files.jsPath],
        {interval: 1000, usePolling: true}, 
        series(
            parallel(scssTask, jsTask)
        )
    );    
}

exports.default = series(
    parallel(scssTask, jsTask), 
    watchTask
);